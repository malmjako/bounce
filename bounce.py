import sys
import math

import pygame

from camera import Camera
from drawables import Rect, Cube, Line
from drawers import CloudDrawer, WireframeDrawer, LogDrawer
import log
from positioning import Orientation


__author__ = 'malmjako'


def main():
    pygame.init()

    window = pygame.display.set_mode((640, 480))

    objects = []
    objects.append(Rect([0, 0, 0], Orientation(), 2, 4, color=pygame.Color('red')))
    objects.append(Rect([0, 0, 2], Orientation(), 2, 4, color=pygame.Color('blue')))

    the_cube = Cube([1, 1, 1], Orientation(), 1, color=pygame.Color('green'))
    cube_rpm = 20
    objects.append(the_cube)

    def bounce_object(obj, amplitude, base, rpm, time_in_milliseconds):
        # Note: 'y' points _down_.
        y = base - amplitude * math.fabs(math.sin(rpm / 2 * 2 * math.pi / 60000 * time_in_milliseconds))
        obj.position[1] = y

    length = 1
    xyz = [Line((0, 0, 0), Orientation(), length, color=pygame.Color('red')),
           Line((0, 0, 0), Orientation().rotate(math.pi/2, 'z'), length, color=pygame.Color('green')),
           Line((0, 0, 0), Orientation().rotate(-math.pi/2, 'y'), length, color=pygame.Color('blue'))]
    objects.extend(xyz)

    camera = Camera([0, 0, -10], Orientation([1, 0, 0], [0, 1, 0]), math.pi / 3, 640, 480)

    drawers = [CloudDrawer(camera, objects, window),
               WireframeDrawer(camera, objects, window),
               LogDrawer(camera, log.get_log(), window)]

    def draw():
        window.fill((0, 0, 0))
        for drawer in drawers:
            drawer.draw()
        pygame.display.flip()

    clock = pygame.time.Clock()
    fps = 60

    # Just run until we get a QUIT event.
    while True:
        milliseconds = clock.tick(fps)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit(0)
            elif event.type in [pygame.KEYDOWN, pygame.KEYUP]:
                camera.process_key(event.key, event.type == pygame.KEYDOWN)
            else:
                pass

        # Let the camera move.
        camera.move(milliseconds)

        # Let the cube have some fun.
        #the_cube.rotate(cube_rpm * 2 * math.pi / 60000 * milliseconds, 'x')
        the_cube.rotate(.5 * cube_rpm * 2 * math.pi / 60000 * milliseconds, 'y')
        #the_cube.rotate(.1 * cube_rpm * 2 * math.pi / 60000 * milliseconds, 'z')
        bounce_object(the_cube, 5, 0, 30, pygame.time.get_ticks())

        draw()

if __name__ == '__main__':
    main()