import numpy as np
from math import pi

__author__ = 'malmjako'

from positioning import Placeable


class Camera(Placeable):
    distance_per_millisecond = 2.0 / 1000
    angle_per_millisecond = 2 * pi / 10 / 1000  # 10 s per revolution

    def __init__(self, position, orientation, viewing_angle, width, height):
        """

        :param position:
        :param orientation: Camera looks in the positive direction of the third dimension, rotated according to the
                            first two.
        :param viewing_angle: Largest viewing angle, in radians, in first orientation dimension.
        :param width: Number of pixels in first orientation dimension.
        :param height: Number of pixels in second orientation dimension.
        """
        super().__init__(position, orientation)
        self.viewing_angle = viewing_angle
        self.width = width
        self.height = height
        self.active_translation = {'x': 0, 'y': 0, 'z': 0}
        self.active_rotation = {'x': 0, 'y': 0, 'z': 0}

    def world2camera(self, p):
        """Transform world point *p* to camera coordinates."""
        return self.orientation.rotation.T.dot(p - self.position)

    def point2frame(self, p):
        """Return position in the camera frame, and distance from camera."""
        # Rotate and translate point to camera coordinates.
        p_camera = self.world2camera(p)

        # Calculate angles in first and second dimensions between point vector and camera direction (third
        # dimension).
        a1 = self.angle_between(np.array((0, 1)), np.array((p_camera[0], p_camera[2])))
        a2 = self.angle_between(np.array((0, 1)), np.array((p_camera[1], p_camera[2])))

        pixels_per_angle = self.width / self.viewing_angle
        x = a1 * pixels_per_angle * (1 - 2 * (p_camera[0] < 0))
        y = a2 * pixels_per_angle * (1 - 2 * (p_camera[1] < 0))

        distance = np.linalg.norm(p_camera)
        pos = (int(x + self.width / 2 + .5), int(y + self.height / 2 + .5))

        return pos, distance

    def in_frame(self, pos):
        x, y = pos
        if ((0 <= x < self.width) and
            (0 <= y < self.height)):
            return True
        else:
            return False

    def log(self):
        import log
        log.log("Camera orientation:\n" +
                           "x: " + str(self.orientation.x.round(2)) + "\n" +
                           "y: " + str(self.orientation.y.round(2)) + "\n" +
                           "z: " + str(self.orientation.z.round(2)) + "\n")
        log.log("Camera position:\n" + str(self.position.round(2)))

    def process_key(self, key, keydown=True):
        """
        Handle key.

        :param key: pygame key.
        :type keydown: bool
        :return: True if key was handled, otherwise false.
        """
        import pygame
        # In / out
        if key == pygame.K_r:
            self.active_translation['z'] = +self.distance_per_millisecond if keydown else 0
        elif key == pygame.K_f:
            self.active_translation['z'] = -self.distance_per_millisecond if keydown else 0
        # Left / right (dimension 1)
        elif key == pygame.K_a:
            self.active_translation['x'] = -self.distance_per_millisecond if keydown else 0
        elif key == pygame.K_d:
            self.active_translation['x'] = +self.distance_per_millisecond if keydown else 0
        # Up / down (dimension 2)
        elif key == pygame.K_w:
            # y-axiz points down when looking in positive z-direction.
            self.active_translation['y'] = -self.distance_per_millisecond if keydown else 0
        elif key == pygame.K_s:
            self.active_translation['y'] = +self.distance_per_millisecond if keydown else 0
        # Rotate around camera direction.
        elif key == pygame.K_q:
            self.active_rotation['z'] = -self.angle_per_millisecond if keydown else 0
        elif key == pygame.K_e:
            self.active_rotation['z'] = +self.angle_per_millisecond if keydown else 0
        # Rotate around x direction.
        elif key == pygame.K_1:
            self.active_rotation['x'] = -self.angle_per_millisecond if keydown else 0
        elif key == pygame.K_z:
            self.active_rotation['x'] = +self.angle_per_millisecond if keydown else 0
        # Rotate around y direction (points down).
        elif key == pygame.K_3:
            self.active_rotation['y'] = +self.angle_per_millisecond if keydown else 0
        elif key == pygame.K_2:
            self.active_rotation['y'] = -self.angle_per_millisecond if keydown else 0
        elif key == pygame.K_l and keydown:
            self.log()
        else:
            return False

        return True

    def move(self, milliseconds):
        for axis, distance_per_millisecond in self.active_translation.items():
            if distance_per_millisecond:
                self.translate(distance_per_millisecond * milliseconds, axis)
        for axis, angle_per_millisecond in self.active_rotation.items():
            if angle_per_millisecond:
                self.rotate(angle_per_millisecond * milliseconds, axis)

    @staticmethod
    def unit_vector(vector):
        """ Returns the unit vector of the vector.  """
        return vector / np.linalg.norm(vector)

    @staticmethod
    def angle_between(v1, v2):
        """ Returns the angle in radians between vectors 'v1' and 'v2'::

                >>> Camera.angle_between((1, 0, 0), (0, 1, 0))
                1.5707963267948966
                >>> Camera.angle_between((1, 0, 0), (1, 0, 0))
                0.0
                >>> Camera.angle_between((1, 0, 0), (-1, 0, 0))
                3.141592653589793
        """
        v1_u = Camera.unit_vector(v1)
        v2_u = Camera.unit_vector(v2)
        angle = np.arccos(np.dot(v1_u, v2_u))
        if np.isnan(angle):
            if (v1_u == v2_u).all():
                return 0.0
            else:
                return np.pi
        return angle