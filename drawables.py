__author__ = 'malmjako'

import numpy as np
import pygame
from positioning import Placeable


class Drawable(Placeable):
    edge_sequence = []
    internal_points = []

    def __init__(self, position, orientation, color=pygame.Color('white')):
        super().__init__(position, orientation)
        self.color = color

    @property
    def external_points(self):
        return [self.position + self.orientation.rotation.dot(p) for p in self.internal_points]

    def get_edges(self):
        edges = []
        points = self.external_points
        for e in self.edge_sequence:
            edges.append((points[e[0]], points[e[1]]))
        return edges


class Line(Drawable):
    edge_sequence = [(0, 1)]

    def __init__(self, position, orientation, length, **kwargs):
        super().__init__(position, orientation, **kwargs)
        self.length = length

    @property
    def internal_points(self):
        return [np.array((0, 0, 0), dtype=np.float32),
                np.array((self.length, 0, 0), dtype=np.float32)]


class Rect(Drawable):
    edge_sequence = [(0, 1), (1, 2), (2, 3), (3, 0)]

    def __init__(self, position, orientation, width, height, **kwargs):
        super().__init__(position, orientation, **kwargs)
        self.width = width
        self.height = height

    @property
    def internal_points(self):
        """Returns a list of `Vector3` objects representing all (non-rotated) corners of the object, relative to object
        center."""
        return [np.array([-self.width / 2.,  self.height / 2., 0]),  # upper left
                np.array([ self.width / 2.,  self.height / 2., 0]),  # upper right
                np.array([ self.width / 2., -self.height / 2., 0]),  # lower right
                np.array([-self.width / 2., -self.height / 2., 0])   # lower left
                ]


class Cube(Drawable):
    edge_sequence = [(0, 1), (1, 2), (2, 3), (3, 0),
        (4, 5), (5, 6), (6, 7), (7, 4),
        (0, 4),
        (1, 5),
        (2, 6),
        (3, 7)]

    def __init__(self, position, orientation, side, **kwargs):
        super().__init__(position, orientation, **kwargs)
        self.side = side
        self.kwargs = kwargs

    @property
    def _rect1(self):
        return Rect(self.position - self.side / 2. * self.orientation.z, self.orientation, self.side, self.side,
                    **self.kwargs)

    @property
    def _rect2(self):
        return Rect(self.position + self.side / 2. * self.orientation.z, self.orientation, self.side, self.side,
                    **self.kwargs)

    @property
    def external_points(self):
        return self._rect1.external_points + self._rect2.external_points