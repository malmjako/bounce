import pygame

__author__ = 'malmjako'


class AbstractDrawer:
    def __init__(self, camera, objects, surface):
        self.camera = camera
        self.objects = objects
        self.surface = surface

    def draw(self):
        raise NotImplementedError


class CloudDrawer(AbstractDrawer):
    SIZE = 5
    CUTOFF = 20

    def draw(self):
        s = pygame.Surface(self.surface.get_size(), pygame.SRCALPHA)
        for o in self.objects:
            for p in o.external_points:
                pos, distance = self.camera.point2frame(p)
                if self.camera.in_frame(pos):
                    r, g, b = o.color.r, o.color.g, o.color.b
                    assert distance >= 0
                    a = int(-255 / self.CUTOFF * distance + 255 + 0.5)
                    if a < 0:
                        a = 0
                    pygame.draw.circle(s, pygame.color.Color(r, g, b, a), pos, self.SIZE)
        self.surface.blit(s, (0, 0))


class WireframeDrawer(AbstractDrawer):
    WIDTH = 1

    def draw(self):
        for o in self.objects:
            for p1, p2 in o.get_edges():
                pos1, _distance = self.camera.point2frame(p1)
                pos2, _distance = self.camera.point2frame(p2)
                if self.camera.in_frame(pos1) or self.camera.in_frame(pos2):
                    pygame.draw.line(self.surface, o.color, pos1, pos2, self.WIDTH)


class LogDrawer(AbstractDrawer):
    SIZE = 16

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.font = pygame.font.Font(None, self.SIZE)

    def draw(self):
        row = 0
        for o in reversed(self.objects):
            for line in o.split('\n'):
                img = self.font.render(line, 1, pygame.Color('white'))
                self.surface.blit(img, (0, row * self.font.get_linesize()))
                row += 1