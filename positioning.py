__author__ = 'malmjako'

import numpy as np


class Orientation(object):
    """Class for orienting cameras and objects."""
    EPSILON = 1e-5

    def __init__(self, x=(1, 0, 0), y=(0, 1, 0)):
        self.x = x
        self.y = y
        # x must be orthogonal to y, or they cannot be used as orientation.
        assert self.x.dot(self.y) < self.EPSILON

    @staticmethod
    def normalize(value):
        """Return normalized NumPy array."""
        return np.array(value, dtype=np.float32) / np.linalg.norm(value)

    def x():
        doc = "The x-direction."
        def fget(self):
            return self._x
        def fset(self, value):
            self._x = self.normalize(value)
        def fdel(self):
            del self._x
        return locals()
    x = property(**x())

    def y():
        doc = "The y-direction."
        def fget(self):
            return self._y
        def fset(self, value):
            self._y = self.normalize(value)
        def fdel(self):
            del self._y
        return locals()
    y = property(**y())

    def z():
        doc = "The z-direction."
        def fget(self):
            # z must be orthogonal to x and y, or they cannot be used as orientation.
            return np.cross(self.x, self.y)
        return locals()
    z = property(**z())

    @property
    def rotation(self):
        """Rotation matrix.

        Examples

        >>> o = Orientation([1, 0, 0], [0, 1, 1])
        >>> v = np.array([1, 0, 0])
        >>> o.rotation.dot(v)
        np.array([[1, 0, 0],
                  [0, 1/math.sqrt(2), 1/math.sqrt(2)],
                  [0, -1/math.sqrt(2), 1/math.sqrt(2))
        """
        return np.array([self.x, self.y, self.z]).T

    def r(self, angle, axis):
        """Calculate rotation matrix for rotation around *axis*.

        See http://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle.

        """
        try:
            # *axis* may have been specified as 'x', 'y', or 'z'.
            u = getattr(self, axis)
        except:
            u = axis
        a = angle
        # Cross product matrix
        cross_m = np.array([[    0, -u[2],  u[1]],
                            [ u[2],     0, -u[0]],
                            [-u[1],  u[0],     0]])
        return np.cos(a) * np.eye(3) + np.sin(a) * cross_m + (1 - np.cos(a)) * np.tensordot(u, u, 0)

    def rotate(self, angle, axis):
        """Rotate *angle* radians around an *axis*.

        >>> o = Orientation([1, 0, 0], [0, 1, 0])
        >>> o.rotate(math.pi / 2, 'x')
        >>> o.rotation.round()
        np.array([[1, 0, 0],
                  [0, 0, 1],
                  [0, -1, 0])

        """
        m = self.r(angle, axis)

        rotated = m.dot(self.rotation)
        self.x = rotated[:, 0]
        self.y = rotated[:, 1]

        return self

    def __repr__(self):
        return repr(self.rotation.round(5))


class Placeable:
    """An object that has a position and orientation, both of which can be manipulated, using methods translate and
    rotate, respectively.

    """

    def __init__(self, position, orientation):
        """

        :param position:
        :param orientation:
        """
        self.position = np.array(position, dtype=np.float32)
        self.orientation = orientation

    def translate(self, distance, axis):
        """Translate the object *distance* units along *axis* (3-d vector or 'x', 'y', or 'z' for translation along the
        object's respective axis).

        """
        try:
            # Might be 'x', 'y', or 'z', in which case, use the orientation of the object.
            axis = getattr(self.orientation, axis)
        except AttributeError:
            pass
        self.position += axis * distance

    def rotate(self, angle, axis):
        """Rotate the object *angle* around *axis* (3-d vector or 'x', 'y', or 'z' for rotation around the object's
        respective axis).

        """
        self.orientation.rotate(angle, axis)